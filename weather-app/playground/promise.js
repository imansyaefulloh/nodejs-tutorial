var asyncAdd = (a, b) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (typeof a === 'number' && typeof b === 'number') {
        resolve(a + b);
      } else {
        reject('Arguments must be numbers');
      }
    }, 2500);
  });
};

asyncAdd(10, 5).then((result) => {
  console.log('Results: ' + result);
  return asyncAdd(result, 33);
}).then((res) => {
  console.log('Results 2: ' + res);
}).catch((error) => {
  console.log('Error:' + error);
});

// var somePromise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     // resolve('Hey, it works');
//     reject('promise did not go well');
//   }, 2500);
// });

// somePromise.then((message) => {
//   console.log('Success ' + message);
// }).catch((error) => {
//   console.log('Error ' + error);
// });