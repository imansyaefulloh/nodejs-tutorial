const utils = require('./utils');
const expect = require('expect');

describe('Utils', () => {
  it('should add two number', () => {
    var result = utils.add(10, 10);
    expect(result).toBe(20).toBeA('number');
  });

  it('should async add two number', (done) => {
    utils.asyncAdd(4, 4, (sum) => {
      expect(sum).toBe(8).toBeA('number');
      done();
    });
  });

  it('should square a number', () => {
    var result = utils.square(10);
    expect(result).toBe(100).toBeA('number');
  });

  it('should async square a number', (done) => {
    utils.asyncSquare(10, (res) => {
      expect(res).toBe(100).toBeA('number');
      done();
    });
  });

  it('should expect some values', () => {
    expect([2, 3, 4]).toExclude(5);

    expect({
      name: 'Iman',
      age: 28
    }).toInclude({
      age: 28
    });
  });

  it('should set firstName and lastName', () => {
    var user = {
      location: 'Indonesia',
      age: 25
    };
    var res = utils.setName(user, 'Iman Syaefulloh');
    // expect(user).toEqual(res);
    expect(res).toInclude({
      firstName: 'Iman',
      lastName: 'Syaefulloh'
    })
  });
});