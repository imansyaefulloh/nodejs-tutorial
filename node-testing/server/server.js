const express = require('express');

var app = express();

app.get('/', (req, res) => {
  res.send('Hello world');
});

app.get('/users', (req, res) => {
  res.send([{
    name: 'Iman',
    age: 28
  }, {
    name: 'Udin',
    age: 20
  }, {
    name: 'Amir',
    age: 45
  }]);
});

app.listen(3000);

module.exports.app = app;